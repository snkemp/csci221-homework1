#include <iostream>
#include <string>
#include <sstream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main() {

	srand(time(NULL));
	//Game values
	int theNumber;
	int numRounds;

	//Player values
	string player_name[2];
	int player_score[2];
	
	//Gets player names
	cout<< "Welcome to the Number Game!!" << "\n\n" << "what is your name Player1?" << endl;
	cin >> player_name[0];
	cout<< "And what is your name Player2?" << endl;
	cin >> player_name[1];

	//Starts player scores;
	player_score[0] = 0;
	player_score[1] = 0;

	//Num rounds
	cout<< "How many rounds would you like to play?" << endl;
	string rounds;
	cin >> rounds;
	stringstream my_converter(rounds);
	if(!(my_converter >> numRounds)){
		numRounds = 1;}

	for(int i = 0; i < numRounds; i++)
	{
		theNumber = (int) (rand()% 20) + 1;
		int round_turn = i%2;

		cout<< "Welcome to round " << (i+1) << ". ";
		cout<< "First to guess a number between 1 and 20 wins." <<endl;
		cout<< player_name[round_turn] << " goes first.\n"<< endl;
		
		bool guessed = false;
		int turn = round_turn;
		while(!guessed)
		{
			cout<< player_name[turn%2]<< ": ";
			int guess;
			string sguess;
			cin >> sguess;
			stringstream ss(sguess);
			if(!(ss >> guess)){
				guess = -1;}

			guessed = (guess == theNumber);
			if(!guessed)
			{
				turn++;
			}
		}
		cout<< "Congratulations " << player_name[turn%2] << " you won round " << (i+1) << "\n";
		player_score[turn%2]++;
	}
	cout<< "Game over. " << player_name[0] << " had " << player_score[0] << " points. ";
	cout<< player_name[1] << " had " << player_score[1] << " points." << endl;

	if(player_score[0] > player_score[1])
		cout<< player_name[0] << " wins!!!!";
	else if(player_score[1] > player_score[0])
		cout<< player_name[1] << " wins!!!!";
	else
		cout<< "It's a tie!!!!";
	cout<< "\n" <<endl;;
	
	return 0;
}
